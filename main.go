package main

import (
	"fmt"
	"log"
	"github.com/bwmarrin/discordgo"
	"io"
	"os"
	"os/exec"
	"os/signal"
)

func streamYTDLOpus(url string, c chan<- []byte) (error) {
	cmd := exec.Command("yt-dlp", "-o", "-", "-x", "--audio-format", "opus", url)

	ytdlOut, err := cmd.StdoutPipe()
	defer ytdlOut.Close()

	if err != nil {
		return err
	}

	err = cmd.Start()

	if err != nil{
		return err
	}

	for {
		bufSize := 16384
		buf := make([]byte, bufSize, bufSize)
		n, err := ytdlOut.Read(buf)
		if err != nil || err != io.EOF {
			return err
		}
		
		if n > 0 {
			c <- buf
		}

		if err == io.EOF && n == 0 {
			break
		}
	}
	return nil
}

func sendYTDLStream(dgv *discordgo.VoiceConnection, url string) error {
	return streamYTDLOpus(url, dgv.OpusSend)
}

var commands = []*discordgo.ApplicationCommand{
	{
		Name: "play",
		Description: "Play some audio",
	},
}

var commandHandlers = map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate){
	"play": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "Hey! This works",
			},
		})
		channel, err := s.Channel(i.ChannelID)
		if err == nil {
			log.Printf("Channel type: %v", channel.Type)
		} else {
			log.Printf("Couldn't find channel")
		}
		userID := i.Member.User.ID
		gid := i.GuildID
		state := s.State
		vs, err := state.VoiceState(gid, userID)
		if err != nil {
			log.Panicf("Error getting voice state for user %v: %v", userID, err)
			return
		}
		if vs == nil {
			log.Printf("No voice state for user : %v", userID)
			return
		}
		cid := vs.ChannelID
		log.Printf("Joining guild %v channel %v", gid, cid)
		dgv, err := s.ChannelVoiceJoin(gid, cid, false, true)
		if err != nil {
			log.Printf("Error joining channel: %v", err)
			return
		}
		go func() {
			dgv.Speaking(true)
			err := sendYTDLStream(dgv, "https://www.youtube.com/watch?v=dQw4w9WgXcQ")
			if err != nil {
				log.Printf("Error sending YTDL stream: %v", err)
			}
			dgv.Speaking(false)
			dgv.Disconnect()
		}()
	},
}

func main() {
	bot_token := os.Getenv("BOT_TOKEN")
	s, err := discordgo.New("Bot " + bot_token)

	if err != nil {
		fmt.Println("Error creating Discord session,", err)
		return
	}
	s.AddHandler(func(dg *discordgo.Session, r *discordgo.Ready) {
		log.Printf("Logged in as: %v#%v", s.State.User.Username, s.State.User.Discriminator)
	})

	s.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsAll)

	if s.Open() != nil {
		log.Panicf("Couldn't open")
		return
	}

	log.Printf("Is state tracking enabled: %v", s.StateEnabled)

	s.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		if h, ok := commandHandlers[i.ApplicationCommandData().Name]; ok {
			h(s, i)
		}
	})

	registeredCommands := make([]*discordgo.ApplicationCommand, len(commands))
	for i, v := range commands {
		cmd, err := s.ApplicationCommandCreate(s.State.User.ID, "", v)
		if err != nil {
			log.Panicf("Cannot create '%v' command: %v", v.Name, err)
		}
		registeredCommands[i] = cmd
	}

	defer s.Close()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	log.Println("Press Ctrl+C to exit")
	<-stop

	for _, v := range registeredCommands {
		err := s.ApplicationCommandDelete(s.State.User.ID, "", v.ID)
		if err != nil {
			log.Printf("Cannot delete '%v' command: %v", v.Name, err)
			return
		}
	}
}
